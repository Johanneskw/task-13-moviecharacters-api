﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;
using Task_13_Movie_Characters_API.DTOs.Actor;
using Task_13_Movie_Characters_API.DTOs.Character;
using Task_13_Movie_Characters_API.Model;

namespace Task_13_Movie_Characters_API.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CharactersController : ControllerBase
    {
        private readonly MoviesDBContext _context;
        private readonly IMapper _mapper;

        public CharactersController(MoviesDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Characters
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterDTO>>> GetCharacters()
        {
            List<Character> characters = await _context.Characters.ToListAsync();
            return Ok(_mapper.Map<List<CharacterDTO>>(characters));
        }

        // GET: api/Characters/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterDTO>> GetCharacter(int id)
        {
            var character = await _context.Characters.FindAsync(id);

            if (character == null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<CharacterDTO>(character));
        }

        // GET: api/Characters/Actor/5
        /// <summary>
        /// Returns all actors to play given character
        /// </summary>
        /// <param name="id"></param>
        /// <returns>actors where characterID == id</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<ActorDTO>>> Actors(int id)
        {
            //Finds all actors who is connected to characterID trough joining table. sorts out only distinct. 
            var actors = await _context.MovieCharacters
                                       .Where(cid => cid.CharacterID == id)
                                       .Select(a => a.Actor)
                                       .Distinct().ToListAsync();
            
            if (actors == null)
            {
                return NotFound();
            }
            //Mapping to actor DTO. 
            var actorsDTO = _mapper.Map<List<ActorDTO>>(actors);

            return Ok(actorsDTO);
        }

        /// <summary>
        /// Gets Character, with list of Titles of all movies Character is featured in. 
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Characters with movies featured in.</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterMovieDTO>> Movie(int id)
        {
            //Gets the character on id, also includes movies it appears in. Then maps to a DTO containing info about character, with list of movietitles. 
            var characters = await _context.Characters
                                           .Include(mc => mc.MovieCharacters)
                                           .ThenInclude(m => m.Movie)
                                           .Where(cid => cid.CharacterID == id).SingleAsync();
            if (characters == null)
            {
                return NotFound();
            }

            var charMovDTO = _mapper.Map<CharacterMovieDTO>(characters);

            return Ok(charMovDTO);
        }


        // PUT: api/Characters/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacter(int id, Character character)
        {
            if (id != character.CharacterID)
            {
                return BadRequest();
            }

            _context.Entry(character).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CharacterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Characters
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Character>> PostCharacter(Character character)
        {
            _context.Characters.Add(character);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCharacter", new { id = character.CharacterID }, character);
        }

        // DELETE: api/Characters/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Character>> DeleteCharacter(int id)
        {
            var character = await _context.Characters.FindAsync(id);
            if (character == null)
            {
                return NotFound();
            }

            _context.Characters.Remove(character);
            await _context.SaveChangesAsync();

            return character;
        }

        private bool CharacterExists(int id)
        {
            return _context.Characters.Any(e => e.CharacterID == id);
        }
    }
}
