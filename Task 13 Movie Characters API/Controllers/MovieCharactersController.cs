﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Task_13_Movie_Characters_API.DTOs.MovieCharacter;
using Task_13_Movie_Characters_API.Model;

namespace Task_13_Movie_Characters_API.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class MovieCharactersController : ControllerBase
    {
        private readonly MoviesDBContext _context;
        private readonly IMapper _mapper;

        public MovieCharactersController(MoviesDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/MovieCharacters
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieCharacterDTO>>> GetMovieCharacters()
        {
            List<MovieCharacter> movieCharacters = await _context.MovieCharacters.ToListAsync();
            return Ok(_mapper.Map<List<MovieCharacterDTO>>(movieCharacters));

        }

        /// <summary>
        /// Gets moviecharacter based on its composite key 
        /// </summary>
        /// <param id="movieID"></param>
        /// <param id="characterID"></param>
        /// <param id="actorID"></param>
        /// <returns>a moviecharacter</returns>
        // GET: api/MovieCharacters/5
        // Gets a MovieCharacter on its composite key of movie, actor and character key. 
        [HttpGet("{movieID}/{characterID}/{actorID}")]
        public async Task<ActionResult<MovieCharacterDTO>> GetMovieCharacter(int movieID, int characterID, int actorID)
        {
            var movieCharacter = await _context.MovieCharacters
                                               .Where(m => m.MovieID == movieID && m.CharacterID == characterID && m.ActorID == actorID).SingleAsync();

            if (movieCharacter == null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<MovieCharacterDTO>(movieCharacter));
        }

        // GET api/MovieCharacters/Actors/5
        /// <summary>
        /// Returns all characters in a movie, and all actors to play characters. 
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Characters and Actors in a Movie</returns>
        /// 
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieCharacterActorDTO>>> Actors(int id)
        {
            //Gets all characters in the specific movie, then includes the actors and characters of the joining class. 
            var characters = await _context.MovieCharacters
                                           .Where(mid => mid.MovieID == id)
                                           .Include(c => c.Character)
                                           .Include(a => a.Actor)
                                           .ToListAsync();
                                               
            if (characters == null)
            {
                return NotFound();
            }
            //Map to a special DTO containing info about both actors and characters.
            var mcaDTO = _mapper.Map<List<MovieCharacterActorDTO>>(characters);

            return mcaDTO;
        }


        // PUT: api/MovieCharacters/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovieCharacter(int id, MovieCharacter movieCharacter)
        {
            if (id != movieCharacter.CharacterID)
            {
                return BadRequest();
            }

            _context.Entry(movieCharacter).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieCharacterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/MovieCharacters
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<MovieCharacter>> PostMovieCharacter(MovieCharacter movieCharacter)
        {
            _context.MovieCharacters.Add(movieCharacter);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (MovieCharacterExists(movieCharacter.CharacterID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetMovieCharacter", new { id = movieCharacter.CharacterID }, movieCharacter);
        }

        // DELETE: api/MovieCharacters/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<MovieCharacter>> DeleteMovieCharacter(int id)
        {
            var movieCharacter = await _context.MovieCharacters.FindAsync(id);
            if (movieCharacter == null)
            {
                return NotFound();
            }

            _context.MovieCharacters.Remove(movieCharacter);
            await _context.SaveChangesAsync();

            return movieCharacter;
        }

        private bool MovieCharacterExists(int id)
        {
            return _context.MovieCharacters.Any(e => e.CharacterID == id);
        }
    }
}
