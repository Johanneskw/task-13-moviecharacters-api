﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Runtime.InteropServices;
using System.Security.Policy;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Connections;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.EntityFrameworkCore.Metadata;
using Task_13_Movie_Characters_API.DTOs.Character;
using Task_13_Movie_Characters_API.DTOs.Franchise;
using Task_13_Movie_Characters_API.DTOs.Movie;
using Task_13_Movie_Characters_API.Model;

namespace Task_13_Movie_Characters_API.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class FranchisesController : ControllerBase
    {
        private readonly MoviesDBContext _context;
        private readonly IMapper _mapper;

        public FranchisesController(MoviesDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
       
        // GET: api/Franchises
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseDTO>>> GetFranchises()
        {
            List<Franchise> franchises = await _context.Franchises.ToListAsync();
            return Ok(_mapper.Map<List<FranchiseDTO>>(franchises));
        }

        // GET: api/Franchises/5
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseDTO>> GetFranchise(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);

            if (franchise == null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<FranchiseDTO>(franchise));
        }

        /// <summary>
        /// Returns all movies in a specific franchise.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Movies where franchiseID == id</returns>
        // GET: api/Franchises/Movies/5
        // Finds and returns a DTO containing all essential info about all movies in a franchise, on the franchise id.
        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<MovieDTO>>> Movie(int id)
        {
            var movies = await _context.Movies.Where(f => f.FranchiseID == id).ToListAsync();

            if (movies == null)
            {
                return NotFound();
            }

            var moviesDTO = _mapper.Map<List<MovieDTO>>(movies);

            return Ok(moviesDTO);
        }

        // GET: api/Franchises/Movies/5
        /// <summary>
        /// Returns all characters in the Franchise
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Charachters in franchise</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<CharacterDTO>>> Characters(int id)
        {
            //from the correct franchise, collects all movies in the franchise, then all characters in all the movies, then removes duplicates. 
            var characters = await _context.Franchises
                                           .Where(f => f.FranchiseID == id)
                                           .SelectMany(m => m.Movies)
                                           .SelectMany(mc => mc.MovieCharacters)
                                           .Select(c => c.Character)
                                           .Distinct().ToListAsync();
            if (characters == null)
            {
                return NotFound();
            }

            var characterDTO = _mapper.Map<List<CharacterDTO>>(characters);

            return Ok(characterDTO);
        }

        /// <summary>
        /// Returns the info about the franchise with list of titles of all movies in franchise (Uses manually configurated DTO).  
        /// </summary>
        /// <param name="id"></param>
        /// <returns>franchise + all movies in franchise</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseMovieDTO>> Movie2(int id)
        {
            var frachise = await _context.Franchises
                                   .Include(m => m.Movies)
                                   .Where(fid => fid.FranchiseID == id)
                                   .SingleAsync();
            
            if (frachise == null) return NotFound();

            var franchiseOTD = _mapper.Map<FranchiseMovieDTO>(frachise);
            return Ok(franchiseOTD);
        }

        // PUT: api/Franchises/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, Franchise franchise)
        {
            if (id != franchise.FranchiseID)
            {
                return BadRequest();
            }

            _context.Entry(franchise).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FranchiseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Franchises
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Franchise>> PostFranchise(Franchise franchise)
        {
            _context.Franchises.Add(franchise);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFranchise", new { id = franchise.FranchiseID }, franchise);
        }

        // DELETE: api/Franchises/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Franchise>> DeleteFranchise(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            if (franchise == null)
            {
                return NotFound();
            }

            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();

            return franchise;
        }

        private bool FranchiseExists(int id)
        {
            return _context.Franchises.Any(e => e.FranchiseID == id);
        }
    }
}
