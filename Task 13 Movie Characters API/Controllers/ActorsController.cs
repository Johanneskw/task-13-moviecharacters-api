﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Task_13_Movie_Characters_API.DTOs.Actor;
using Task_13_Movie_Characters_API.DTOs.Character;
using Task_13_Movie_Characters_API.DTOs.Movie;
using Task_13_Movie_Characters_API.Model;

namespace Task_13_Movie_Characters_API.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ActorsController : ControllerBase
    {
        private readonly MoviesDBContext _context;
        private readonly IMapper _mapper;

        public ActorsController(MoviesDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        //Gets all actors 
        // GET: api/Actors
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ActorDTO>>> GetActors()
        {
            List<Actor> actor = await _context.Actors.ToListAsync();
            return Ok(_mapper.Map<List<ActorDTO>>(actor));
        }

        //Gets actor by id 
        // GET: api/Actors/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ActorDTO>> GetActor(int id)
        {
            var actor = await _context.Actors.FindAsync(id);

            if (actor == null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<ActorDTO>(actor));
        }

        // GET: api/Actors/Characters/5
        /// <summary>
        /// Gets all characters an actor has played
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Characters where actorID == id </returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<CharacterDTO>>> Character(int id)
        {
            //Gets all characters, does not include duplictaes. 
            var characters = await _context.MovieCharacters
                                           .Where(a => a.ActorID == id)
                                           .Select(c => c.Character)
                                           .Distinct().ToListAsync();
            if (characters == null)
            {
                return NotFound();
            }
            //Mapping character to DTO, which only contains essential info. 
            var characterDTO = _mapper.Map<List<Character>, List<CharacterDTO>>(characters);

            return Ok(characterDTO);
        }

        //GET: api/Actors/Movies/5
        /// <summary>
        /// Returns all Movies a character has played in
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Movies where actorID == id</returns>
        //Gets all movies an actor has played in, and maps to movieDTO
        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<MovieDTO>>> Movies(int id)
        {
            var movies = await _context.MovieCharacters
                                       .Where(aid => aid.ActorID == id)
                                       .Select(m => m.Movie).ToListAsync();

            if (movies == null)
            {
                return NotFound();
            }
            
            var moviesDTO = _mapper.Map<List<MovieDTO>>(movies);

            return Ok(moviesDTO);
        }

        /// <summary>
        /// Does the same as the other Actor/Character, but uses DTO to return only name of character, and manually configured mapping. 
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Characters played by Actor </returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<ActorCharacterDTO>>> Character2(int id)
        {
            //Does the same as the other function, but also includes info about the actor playing the character.
            var actors = await _context.Actors
                                       .Include(mc => mc.MovieCharacters)
                                       .ThenInclude(c => c.Character)
                                       .Where(aid => aid.ActorID == id).ToListAsync();
            if (actors == null)
            {
                return NotFound();
            }

            var actorsCharsDTO = _mapper.Map<List<ActorCharacterDTO>>(actors);

            return Ok(actorsCharsDTO);
        }


        // PUT: api/Actors/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutActor(int id, Actor actor)
        {
            if (id != actor.ActorID)
            {
                return BadRequest();
            }

            _context.Entry(actor).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ActorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Actors
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Actor>> PostActor(Actor actor)
        {
            _context.Actors.Add(actor);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetActor", new { id = actor.ActorID }, actor);
        }

        // DELETE: api/Actors/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Actor>> DeleteActor(int id)
        {
            var actor = await _context.Actors.FindAsync(id);
            if (actor == null)
            {
                return NotFound();
            }

            _context.Actors.Remove(actor);
            await _context.SaveChangesAsync();

            return actor;
        }

        private bool ActorExists(int id)
        {
            return _context.Actors.Any(e => e.ActorID == id);
        }
    }
}
