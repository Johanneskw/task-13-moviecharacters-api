﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task_13_Movie_Characters_API.DTOs.Director
{
    public class DirectorDTO
    {
        public int DirectorID { get; set; }
        public string Firsname { get; set; }
        public string Lastname { get; set; }
        public DateTime DOB { get; set; }
    }
}
