﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task_13_Movie_Characters_API.Model;

namespace Task_13_Movie_Characters_API.DTOs.Actor
{
    //Basic information about actor, also features its characters. 
    public class ActorCharacterDTO
    {
        public int ActorID { get; set; }
        public string Firstname { get; set; }
        public string OtherName { get; set; }
        public string Lastname { get; set; }
        public List<string> Characters { get; set; }
    }
}
