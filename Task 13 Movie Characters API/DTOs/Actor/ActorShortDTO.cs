﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task_13_Movie_Characters_API.DTOs.Actor
{
    public class ActorShortDTO
    {
        public int ActorID { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public DateTime DOB { get; set; }
    }
}
