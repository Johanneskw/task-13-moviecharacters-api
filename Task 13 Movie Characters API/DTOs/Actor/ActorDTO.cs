﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task_13_Movie_Characters_API.DTOs.Actor
{
    //Essential actor information. 
    public class ActorDTO
    {
        public int ActorID { get; set; }
        public string Firstname { get; set; }
        public string OtherName { get; set; }
        public string Lastname { get; set; }
        public DateTime DOB { get; set; }
        public string POB { get; set; }
    }
}
