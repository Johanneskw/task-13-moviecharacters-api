﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task_13_Movie_Characters_API.Model;

namespace Task_13_Movie_Characters_API.DTOs.Character
{
    //Displays basic character-info, and list with titles of all movies featuered in.
    public class CharacterMovieDTO
    {
        public int CharacterID { get; set; }
        public string FullName { get; set; }
        public string Alias { get; set; }
        public List<string> Movies { get; set; }
    }
}
