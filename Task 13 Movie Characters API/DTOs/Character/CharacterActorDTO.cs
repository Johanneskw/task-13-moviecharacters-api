﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task_13_Movie_Characters_API.DTOs.Actor;

namespace Task_13_Movie_Characters_API.DTOs.Character
{
    //Character info, and an DTO with info about the actor playing the character.
    public class CharacterActorDTO 
    {
        public int CharacterID { get; set; }
        public string FullName { get; set; }
        public string Alias { get; set; }
        public ActorDTO Actor { get; set; }
    }
}
