﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task_13_Movie_Characters_API.DTOs.Character
{
    //Basic Charcter info. 
    public class CharacterDTO
    {
        public int CharacterID { get; set; }
        public string FullName { get; set; }
        public string Alias { get; set; }
    }
}
