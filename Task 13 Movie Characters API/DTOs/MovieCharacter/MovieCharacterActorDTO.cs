﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task_13_Movie_Characters_API.DTOs.Actor;
using Task_13_Movie_Characters_API.DTOs.Character;

namespace Task_13_Movie_Characters_API.DTOs.MovieCharacter
{
    //Provides the Characters of a movie, and the actors to play them, trough DTOs with actor and character info. 
    public class MovieCharacterActorDTO
    {
        public ActorDTO Actor { get; set; }
        public CharacterDTO Character { get; set; }
    }
}
