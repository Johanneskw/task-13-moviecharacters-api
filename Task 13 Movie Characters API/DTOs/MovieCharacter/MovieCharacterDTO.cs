﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task_13_Movie_Characters_API.DTOs.Actor;
using Task_13_Movie_Characters_API.DTOs.Character;
using Task_13_Movie_Characters_API.DTOs.Movie;

namespace Task_13_Movie_Characters_API.DTOs.MovieCharacter
{
    public class MovieCharacterDTO
    {
        public int MovieID { get; set; }
        public MovieDTO Movie { get; set; }
        public int CharacterID { get; set; }
        public CharacterDTO Character { get; set; }
        public int ActorID { get; set; }
        public ActorShortDTO Actor { get; set; }
    }
}
