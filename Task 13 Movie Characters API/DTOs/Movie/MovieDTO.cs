﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task_13_Movie_Characters_API.DTOs.Movie
{
    //Most basic movie information. 
    public class MovieDTO
    {
        public int MovieID { get; set; }
        public string Title { get; set; }
        public string Genres { get; set; }
        public DateTime ReleaseYear { get; set; }
    }
}
