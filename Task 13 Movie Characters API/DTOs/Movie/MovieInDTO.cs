﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task_13_Movie_Characters_API.DTOs.Movie
{
    public class MovieInDTO
    {
        public int MovieID { get; set; }
        public string Title { get; set; }
        public string Genres { get; set; }
        public DateTime ReleaseYear { get; set; }
        public int DirectorID { get; set; }
        public int FranchiseID { get; set; }
    }
}
