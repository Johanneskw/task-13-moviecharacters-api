﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task_13_Movie_Characters_API.DTOs.Franchise
{
    //Basic information about Franchise
    public class FranchiseDTO
    {
        public int FranchiseID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
