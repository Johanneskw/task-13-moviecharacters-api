﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task_13_Movie_Characters_API.DTOs.Franchise
{
    //Franchise info + list of titles of all movies in franchise. 
    public class FranchiseMovieDTO
    { 
        public int FranchiseID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<string> Movie { get; set; }
    }
}
