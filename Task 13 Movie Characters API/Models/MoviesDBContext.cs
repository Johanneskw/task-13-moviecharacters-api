﻿using Microsoft.AspNetCore.Http.Connections;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task_13_Movie_Characters_API.Model
{
    public class MoviesDBContext : DbContext
    {
        //Creating all db schemas
        public DbSet<Actor> Actors { get; set; }
        public DbSet<Character> Characters { get; set; }
        public DbSet<MovieCharacter> MovieCharacters { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Director> Directors { get; set; }
        public DbSet<Franchise> Franchises { get; set; }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    optionsBuilder.UseSqlServer(@"Data Source=PC7385\SQLEXPRESS;Initial Catalog=MoviesDB;Integrated Security=True");
        //}
        public MoviesDBContext(DbContextOptions<MoviesDBContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Setting one to many relations between all movies, characters and actors, and the joiningclass, for the many to many relations. 
            modelBuilder.Entity<MovieCharacter>().HasKey(mc => new { mc.CharacterID, mc.ActorID, mc.MovieID });
            modelBuilder.Entity<MovieCharacter>()
                .HasOne(c => c.Character)
                .WithMany(mc => mc.MovieCharacters)
                .HasForeignKey(cid => cid.CharacterID);
            modelBuilder.Entity<MovieCharacter>()
                .HasOne(m => m.Movie)
                .WithMany(mc => mc.MovieCharacters)
                .HasForeignKey(mid => mid.MovieID);
            modelBuilder.Entity<MovieCharacter>()
                .HasOne(a => a.Actor)
                .WithMany(mc => mc.MovieCharacters)
                .HasForeignKey(aid => aid.ActorID);

            //Seeding data, Actors:
            modelBuilder.Entity<Actor>().HasData(new Actor
            {
                ActorID = 1,
                Firstname = "Elijah",
                Lastname = "Wood",
                Gender = 'M',
                DOB = new DateTime(1981, 01, 28),
                POB = "Iowa, USA",
                Biography = "Elijah Wood is an American actor best known for portraying Frodo Baggins in " +
                "Peter Jackson's blockbuster Lord of the Rings film trilogy. In addition to reprising the role in The Hobbit series, " +
                "Wood also played Ryan in the FX television comedy Wilfred (2011) and voiced Beck in the Disney XD animated television series TRON: Uprising (2012).",
                Picture = "https://m.media-amazon.com/images/M/MV5BMTM0NDIxMzQ5OF5BMl5BanBnXkFtZTcwNzAyNTA4Nw@@._V1_SY1000_CR0,0,666,1000_AL_.jpg"
            });
            modelBuilder.Entity<Actor>().HasData(new Actor
            {
                ActorID = 2,
                Firstname = "Orlando",
                Lastname = "Bloom",
                Gender = 'M',
                DOB = new DateTime(1977, 01, 13),
                POB = "Kent, England",
                Biography = "Orlando Jonathan Blanchard Bloom was born in Canterbury, Kent, England on January 13, 1977. " +
                "His mother, Sonia Constance Josephine (Copeland), was born in Kolkata, India, to an English family then-resident there. " +
                "The man he first knew as his father, Harry Bloom, was a legendary political activist who fought for civil rights in South Africa.",
                Picture = "https://m.media-amazon.com/images/M/MV5BMjE1MDkxMjQ3NV5BMl5BanBnXkFtZTcwMzQ3Mjc4MQ@@._V1_.jpg"
            });
            modelBuilder.Entity<Actor>().HasData(new Actor
            {
                ActorID = 3,
                Firstname = "Viggo",
                Lastname = "Mortensen",
                Gender = 'M',
                DOB = new DateTime(1958, 10, 20),
                POB = "New York, USA",
                Biography = "Since his screen debut as a young Amish farmer in Peter Weir's Witness (1985), Viggo Mortensen's career has been marked " +
                "by a steady string of well-rounded performances. Mortensen was born in New York City, to Grace Gamble (Atkinson) and " +
                "Viggo Peter Mortensen, Sr. His father was Danish, his mother was American.",
                Picture = "https://m.media-amazon.com/images/M/MV5BNDQzOTg4NzA2Nl5BMl5BanBnXkFtZTcwMzkwNjkxMg@@._V1_.jpg"
            });
            modelBuilder.Entity<Actor>().HasData(new Actor
            {
                ActorID = 4,
                Firstname = "Richard",
                Lastname = "Harris",
                Gender = 'M',
                DOB = new DateTime(1930, 10, 01),
                POB = "Limerick, Ireland",
                Biography = "Richard St John Harris was born on October 1, 1930 in Limerick, Ireland, to a farming family, one of nine children born to " +
                "Mildred Josephine (nee Harty) and Ivan John Harris. He attended Crescent College, a Jesuit school, and was an " +
                "excellent rugby player, with a strong passion for literature.",
                Picture = "https://m.media-amazon.com/images/M/MV5BMTgzNTA5ODg1NV5BMl5BanBnXkFtZTcwMDU3MTU5Mw@@._V1_.jpg"
            });
            modelBuilder.Entity<Actor>().HasData(new Actor
            {
                ActorID = 5,
                Firstname = "Daniel",
                Lastname = "Radcliffe",
                Gender = 'M',
                DOB = new DateTime(1989, 07, 23),
                POB = "Fulham, England",
                Biography = "Daniel Jacob Radcliffe was born on July 23, 1989 in Fulham, London, England, to casting agent Marcia Gresham " +
                "(née Jacobson) and literary agent Alan Radcliffe. His father is from a Northern Irish Protestant background, while his mother " +
                "was born in South Africa, to a Jewish family (from Lithuania, Poland, Russia, and Germany).",
                Picture = "https://m.media-amazon.com/images/M/MV5BMTg4NTExODc3Nl5BMl5BanBnXkFtZTgwODUyMDEzMDE@._V1_.jpg"
            });
            modelBuilder.Entity<Actor>().HasData(new Actor
            {
                ActorID = 6,
                Firstname = "Michael",
                Lastname = "Gambon",
                Gender = 'M',
                DOB = new DateTime(1940, 10, 19),
                POB = "Dublin, Ireland",
                Biography = "Sir Michael Gambon was born in Cabra, Dublin, Ireland, to Mary (Hoare), a seamstress, and Edward Gambon, an engineer. " +
                "After joining the National Theatre, under the Artistic Directorship of Sir Laurence Olivier, " +
                "Gambon went on to appear in a number of leading roles in plays written by Alan Ayckbourn.",
                Picture = "https://m.media-amazon.com/images/M/MV5BMTY3OTc4MTgyN15BMl5BanBnXkFtZTcwNTAxNjA3Mg@@._V1_.jpg"
            });
            //Characters: 
            modelBuilder.Entity<Character>().HasData(new Character
            {
                CharacterID = 1,
                FullName = "Frodo Baggins",
                Alias = "Frodo, Mr. Underhill",
                Gender = 'M',
                Picture = "https://dunesjedi.files.wordpress.com/2019/01/frodo_0.jpg?w=1108"
            });
            modelBuilder.Entity<Character>().HasData(new Character
            {
                CharacterID = 2,
                FullName = "Legolas",
                Alias = "Greenleaf",
                Gender = 'M',
                Picture = "https://vignette.wikia.nocookie.net/lotr/images/3/33/Legolas_-_in_Two_Towers.PNG/revision/latest/top-crop/width/360/height/450?cb=20120916035151"
            });
            modelBuilder.Entity<Character>().HasData(new Character
            {
                CharacterID = 3,
                FullName = "Aragorn II Elessar Telcontar",
                Alias = "Elessar, Telcontar, Thorongil, Estel",
                Gender = 'M',
                Picture = "https://vignette.wikia.nocookie.net/lotr/images/b/b6/Aragorn_profile.jpg/revision/latest?cb=20170121121423"
            });
            modelBuilder.Entity<Character>().HasData(new Character
            {
                CharacterID = 4,
                FullName = "Albus Percival Wulfric Brian Dumbledore",
                Alias = "Your Headship, Professorhead, Dumbly - dorr, Crook - nosed, Muggle - loving fool, Crackpot old fool",
                Gender = 'M',
                Picture = "https://i.pinimg.com/236x/38/fd/53/38fd53a85ba7d2431698599ba147239b--david-albus-dumbledore.jpg"
            });
            modelBuilder.Entity<Character>().HasData(new Character
            {
                CharacterID = 5,
                FullName = "Harry James Potter",
                Alias = "The Boy Who Lived, The Chosen One",
                Gender = 'M',
                Picture = "https://media.harrypotterfanzone.com/spellbook-thomas-taylor-illustration.jpg"
            });
            //Franchises
            modelBuilder.Entity<Franchise>().HasData(new Franchise
            {
                FranchiseID = 1, 
                Name = "Lord of The Rings",
                Description = "The Lord of the Rings is a film series of three epic fantasy adventure films directed by Peter Jackson, " +
                "based on the novel written by J. R. R. Tolkien.",
            });
            modelBuilder.Entity<Franchise>().HasData(new Franchise
            {
                FranchiseID = 2,
                Name = "Harry Potter",
                Description = "Harry Potter is a film series based on the eponymous novels by author J. K. Rowling. " +
                "The series is distributed by Warner Bros. and consists of eight fantasy films, beginning with Harry Potter and the Philosopher's Stone (2001) " +
                "and culminating with Harry Potter and the Deathly Hallows – Part 2 (2011).",
            });
            //Directors
            modelBuilder.Entity<Director>().HasData(new Director
            {
                DirectorID = 1,
                Firsname = "Peter",
                Lastname = "Jackson",
                DOB = new DateTime(1961, 10,31)
            });
            modelBuilder.Entity<Director>().HasData(new Director
            {
                DirectorID = 2,
                Firsname = "Chris",
                Lastname = "Columbus",
                DOB = new DateTime(1958, 09, 10)
            });
            modelBuilder.Entity<Director>().HasData(new Director
            {
                DirectorID = 3,
                Firsname = "David",
                Lastname = "Yates",
                DOB = new DateTime(1963, 10, 08)
            });
            //Movies
            modelBuilder.Entity<Movie>().HasData(new Movie
            {
                MovieID = 1,
                Title = "The Lord of the Rings: The Fellowship of the Ring",
                Genres = "Action,Adventure,Drama,Fantasy",
                ReleaseYear = new DateTime(2001, 12, 19),
                DirectorID = 1, 
                Picture = "https://m.media-amazon.com/images/M/MV5BN2EyZjM3NzUtNWUzMi00MTgxLWI0NTctMzY4M2VlOTdjZWRiXkEyXkFqcGdeQXVyNDUzOTQ5MjY@._V1_SY999_CR0,0,673,999_AL_.jpg",
                Trailer = "https://www.youtube.com/watch?v=V75dMMIW2B4",
                FranchiseID = 1
            });
            modelBuilder.Entity<Movie>().HasData(new Movie
            {
                MovieID = 2,
                Title = "The Lord of the Rings: The Two Towers",
                Genres = "Action,Adventure,Drama,Fantasy",
                ReleaseYear = new DateTime(2002, 12, 18),
                DirectorID = 1,
                Picture = "https://m.media-amazon.com/images/M/MV5BZGMxZTdjZmYtMmE2Ni00ZTdkLWI5NTgtNjlmMjBiNzU2MmI5XkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY1000_CR0,0,642,1000_AL_.jpg",
                Trailer = "https://youtu.be/hYcw5ksV8YQ",
                FranchiseID = 1
            });
            modelBuilder.Entity<Movie>().HasData(new Movie
            {
                MovieID = 3,
                Title = "The Lord of the Rings: The Return of the King",
                Genres = "Action,Adventure,Drama,Fantasy",
                ReleaseYear = new DateTime(2003, 12, 17),
                DirectorID = 1,
                Picture = "https://m.media-amazon.com/images/M/MV5BNzA5ZDNlZWMtM2NhNS00NDJjLTk4NDItYTRmY2EwMWZlMTY3XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_SY1000_CR0,0,675,1000_AL_.jpg",
                Trailer = "https://youtu.be/r5X-hFf6Bwo",
                FranchiseID = 1
            });
            modelBuilder.Entity<Movie>().HasData(new Movie
            {
                MovieID = 4,
                Title = "Harry Potter and the Sorcerer's Stone",
                Genres = "Adventure,Family,Fantasy",
                ReleaseYear = new DateTime(2001, 11, 16),
                DirectorID = 2,
                Picture = "https://m.media-amazon.com/images/M/MV5BNjQ3NWNlNmQtMTE5ZS00MDdmLTlkZjUtZTBlM2UxMGFiMTU3XkEyXkFqcGdeQXVyNjUwNzk3NDc@._V1_.jpg",
                Trailer = "https://youtu.be/VyHV0BRtdxo",
                FranchiseID = 2
            });
            modelBuilder.Entity<Movie>().HasData(new Movie
            {
                MovieID = 5,
                Title = "Harry Potter and the Deathly Hallows: Part 2",
                Genres = "Adventure,Family,Fantasy",
                ReleaseYear = new DateTime(2011, 07, 15),
                DirectorID = 3,
                Picture = "https://m.media-amazon.com/images/M/MV5BMjIyZGU4YzUtNDkzYi00ZDRhLTljYzctYTMxMDQ4M2E0Y2YxXkEyXkFqcGdeQXVyNTIzOTk5ODM@._V1_SX667_CR0,0,667,999_AL_.jpg",
                Trailer = "https://youtu.be/mObK5XD8udk",
                FranchiseID = 2
            });
            //Moviecharacters
            modelBuilder.Entity<MovieCharacter>().HasData(new MovieCharacter
            {
                MovieID = 1,
                CharacterID = 1,
                ActorID = 1, 
                Picture = "https://giantbomb1.cbsistatic.com/uploads/scale_small/3/39164/1245387-untitled_1.png"
            });
            modelBuilder.Entity<MovieCharacter>().HasData(new MovieCharacter
            {
                MovieID = 2,
                CharacterID = 1,
                ActorID = 1,
                Picture = "https://giantbomb1.cbsistatic.com/uploads/scale_small/3/39164/1245387-untitled_1.png"
            });
            modelBuilder.Entity<MovieCharacter>().HasData(new MovieCharacter
            {
                MovieID = 3,
                CharacterID = 1,
                ActorID = 1,
                Picture = "https://giantbomb1.cbsistatic.com/uploads/scale_small/3/39164/1245387-untitled_1.png"
            });
            modelBuilder.Entity<MovieCharacter>().HasData(new MovieCharacter
            {
                MovieID = 1,
                CharacterID = 2,
                ActorID = 2,
                Picture = "https://i.pinimg.com/originals/04/80/29/048029f362c484a2a46b928afbe98837.jpg"
            });
            modelBuilder.Entity<MovieCharacter>().HasData(new MovieCharacter
            {
                MovieID = 2,
                CharacterID = 2,
                ActorID = 2,
                Picture = "https://i.pinimg.com/originals/04/80/29/048029f362c484a2a46b928afbe98837.jpg"
            });
            modelBuilder.Entity<MovieCharacter>().HasData(new MovieCharacter
            {
                MovieID = 3,
                CharacterID = 2,
                ActorID = 2,
                Picture = "https://i.pinimg.com/originals/04/80/29/048029f362c484a2a46b928afbe98837.jpg"
            });
            modelBuilder.Entity<MovieCharacter>().HasData(new MovieCharacter
            {
                MovieID = 1,
                CharacterID = 3,
                ActorID = 3,
                Picture = "https://cdn.vox-cdn.com/thumbor/06nk-xAPGzm1jbuYDkucCbNnbzA=/1400x1400/filters:format(jpeg)/cdn.vox-cdn.com/uploads/chorus_asset/file/10856645/aragorn.jpg"
            });
            modelBuilder.Entity<MovieCharacter>().HasData(new MovieCharacter
            {
                MovieID = 2,
                CharacterID = 3,
                ActorID = 3,
                Picture = "https://cdn.vox-cdn.com/thumbor/06nk-xAPGzm1jbuYDkucCbNnbzA=/1400x1400/filters:format(jpeg)/cdn.vox-cdn.com/uploads/chorus_asset/file/10856645/aragorn.jpg"
            });
            modelBuilder.Entity<MovieCharacter>().HasData(new MovieCharacter
            {
                MovieID = 3,
                CharacterID = 3,
                ActorID = 3,
                Picture = "https://cdn.vox-cdn.com/thumbor/06nk-xAPGzm1jbuYDkucCbNnbzA=/1400x1400/filters:format(jpeg)/cdn.vox-cdn.com/uploads/chorus_asset/file/10856645/aragorn.jpg"
            });
            modelBuilder.Entity<MovieCharacter>().HasData(new MovieCharacter
            {
                MovieID = 4,
                CharacterID = 4,
                ActorID = 4,
                Picture = "https://i.ytimg.com/vi/5MzQifNc_Oo/maxresdefault.jpg"
            });
            modelBuilder.Entity<MovieCharacter>().HasData(new MovieCharacter
            {
                MovieID = 4,
                CharacterID = 5,
                ActorID = 5,
                Picture = "https://seasideblogsong.files.wordpress.com/2014/05/harry-potter.jpg"
            });
            modelBuilder.Entity<MovieCharacter>().HasData(new MovieCharacter
            {
                MovieID = 5,
                CharacterID = 4,
                ActorID = 6,
                Picture = "https://cdn3.whatculture.com/images/2016/09/5c633db213a54ab2-600x338.jpg"
            });
            modelBuilder.Entity<MovieCharacter>().HasData(new MovieCharacter
            {
                MovieID = 5,
                CharacterID = 5,
                ActorID = 5,
                Picture = "https://hips.hearstapps.com/digitalspyuk.cdnds.net/11/21/550w_movies_harry_potter_it_all_ends.jpg"
            });
        }
    }
}
