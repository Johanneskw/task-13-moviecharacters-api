﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task_13_Movie_Characters_API.Model
{
    //Holds foreign keys to both parts of the relation, movies and character.
    public class MovieCharacter
    {
        public int MovieCharacterID { get; set; }
        public int MovieID { get; set; }
        public Movie Movie { get; set; }
        public int CharacterID { get; set; }
        public Character Character { get; set; }
        public string Picture { get; set; }
        public Actor Actor { get; set; }
        public int ActorID { get; set; }
    }
}
