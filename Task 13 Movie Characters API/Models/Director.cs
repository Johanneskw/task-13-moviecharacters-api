﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task_13_Movie_Characters_API.Model
{
    //Holds list of movies, as director can direct several movies, many to one as movie can only have one director(per now).
    public class Director
    {
        public int DirectorID { get; set; }
        public string Firsname { get; set; }
        public string Lastname { get; set; }
        public DateTime DOB { get; set; }
        public List<Movie> Movies { get; set; }
    }
}
