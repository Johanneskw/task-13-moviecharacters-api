﻿using System;
using System.Collections.Generic;

namespace Task_13_Movie_Characters_API.Model
{
    //Holds foreign key to the direktor of the movie, and Franchice. both are one to many, one movie can only have 
    //one director and be in one franchise, but a director can direct several movies, and a franchise contain several aswell. 
    public class Movie
    {
        public int MovieID { get; set; }
        public string Title { get; set; }
        public string Genres { get; set; }
        public DateTime ReleaseYear { get; set; }
        public Director Director { get; set; }
        public int DirectorID { get; set; }
        public List<MovieCharacter> MovieCharacters { get; set; }
        public string Picture { get; set; }
        public string Trailer { get; set; }
        public Franchise franchise { get; set; }
        public int FranchiseID { get; set; }

    }
}
