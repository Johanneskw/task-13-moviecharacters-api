﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task_13_Movie_Characters_API.Model
{
    public class Character
    {
        public int CharacterID { get; set; }
        public string FullName { get; set; }
        public string Alias { get; set; }
        public char Gender { get; set; }
        public string Picture { get; set; }
        public List<MovieCharacter> MovieCharacters { get; set; }

    }
}
