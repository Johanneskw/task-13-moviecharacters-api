﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task_13_Movie_Characters_API.DTOs.Franchise;
using Task_13_Movie_Characters_API.Model;

namespace Task_13_Movie_Characters_API.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Franchise, FranchiseDTO>().ReverseMap();
            //Gets the movietitles from movies connected to the franchise in the db. 
            CreateMap<Franchise, FranchiseMovieDTO>().ForMember(fm => fm.Movie, mm => mm.MapFrom(f => f.Movies.Select(m => m.Title).ToList()));
        }
    }
}
