﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task_13_Movie_Characters_API.DTOs.Actor;
using Task_13_Movie_Characters_API.Model;

namespace Task_13_Movie_Characters_API.Profiles
{
    public class ActorProfile : Profile
    {
        public ActorProfile()
        {
            CreateMap<Actor, ActorDTO>().ReverseMap();
            CreateMap<Actor, ActorShortDTO>().ReverseMap();
            //Gets name of all characters played by the actor.
            CreateMap<Actor, ActorCharacterDTO>().ForMember(ac => ac.Characters, opt => opt.MapFrom(mc => mc.MovieCharacters.Select(ch => ch.Character.FullName).Distinct().ToList()));
        }
    }
}
