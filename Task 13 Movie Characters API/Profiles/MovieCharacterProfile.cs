﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task_13_Movie_Characters_API.DTOs.Actor;
using Task_13_Movie_Characters_API.DTOs.Character;
using Task_13_Movie_Characters_API.DTOs.Movie;
using Task_13_Movie_Characters_API.DTOs.MovieCharacter;
using Task_13_Movie_Characters_API.Model;

namespace Task_13_Movie_Characters_API.Profiles
{
    public class MovieCharacterProfile : Profile
    {
        public MovieCharacterProfile()
        {
            CreateMap<MovieCharacter, MovieCharacterActorDTO>();
            CreateMap<MovieCharacterActorDTO, ActorDTO>();
            CreateMap<MovieCharacterActorDTO, CharacterDTO>();

            CreateMap<MovieCharacter, MovieCharacterDTO>();
            CreateMap<MovieCharacterDTO, MovieDTO>();
            CreateMap<MovieCharacterDTO, CharacterDTO>();
            CreateMap<MovieCharacterDTO, ActorShortDTO>();

        }
    }
}
