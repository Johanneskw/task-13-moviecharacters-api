﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task_13_Movie_Characters_API.DTOs.Actor;
using Task_13_Movie_Characters_API.DTOs.Character;
using Task_13_Movie_Characters_API.Model;

namespace Task_13_Movie_Characters_API.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            CreateMap<Character, CharacterDTO>().ReverseMap();
            //basic Character data, but also gets the titles of movies the character feautures in, as a list of strings.
            CreateMap<Character, CharacterMovieDTO>().ForMember(ch => ch.Movies, opt => opt.MapFrom(mc => mc.MovieCharacters.Select(m => m.Movie.Title).ToList()));

            CreateMap<Character, CharacterActorDTO>();
            CreateMap<CharacterActorDTO, ActorDTO>();
        }
    }
}
