﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Task_13_Movie_Characters_API.Migrations
{
    public partial class AddingSeedingData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Actors",
                columns: new[] { "ActorID", "Biography", "DOB", "Firstname", "Gender", "Lastname", "OtherName", "POB", "Picture" },
                values: new object[,]
                {
                    { 1, "Elijah Wood is an American actor best known for portraying Frodo Baggins in Peter Jackson's blockbuster Lord of the Rings film trilogy. In addition to reprising the role in The Hobbit series, Wood also played Ryan in the FX television comedy Wilfred (2011) and voiced Beck in the Disney XD animated television series TRON: Uprising (2012).", new DateTime(1981, 1, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), "Elijah", "M", "Wood", null, "Iowa, USA", "https://m.media-amazon.com/images/M/MV5BMTM0NDIxMzQ5OF5BMl5BanBnXkFtZTcwNzAyNTA4Nw@@._V1_SY1000_CR0,0,666,1000_AL_.jpg" },
                    { 2, "Orlando Jonathan Blanchard Bloom was born in Canterbury, Kent, England on January 13, 1977. His mother, Sonia Constance Josephine (Copeland), was born in Kolkata, India, to an English family then-resident there. The man he first knew as his father, Harry Bloom, was a legendary political activist who fought for civil rights in South Africa.", new DateTime(1977, 1, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), "Orlando", "M", "Bloom", null, "Kent, England", "https://m.media-amazon.com/images/M/MV5BMjE1MDkxMjQ3NV5BMl5BanBnXkFtZTcwMzQ3Mjc4MQ@@._V1_.jpg" },
                    { 3, "Since his screen debut as a young Amish farmer in Peter Weir's Witness (1985), Viggo Mortensen's career has been marked by a steady string of well-rounded performances. Mortensen was born in New York City, to Grace Gamble (Atkinson) and Viggo Peter Mortensen, Sr. His father was Danish, his mother was American.", new DateTime(1958, 10, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), "Viggo", "M", "Mortensen", null, "New York, USA", "https://m.media-amazon.com/images/M/MV5BNDQzOTg4NzA2Nl5BMl5BanBnXkFtZTcwMzkwNjkxMg@@._V1_.jpg" },
                    { 4, "Richard St John Harris was born on October 1, 1930 in Limerick, Ireland, to a farming family, one of nine children born to Mildred Josephine (nee Harty) and Ivan John Harris. He attended Crescent College, a Jesuit school, and was an excellent rugby player, with a strong passion for literature.", new DateTime(1930, 10, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Richard", "M", "Harris", null, "Limerick, Ireland", "https://m.media-amazon.com/images/M/MV5BMTgzNTA5ODg1NV5BMl5BanBnXkFtZTcwMDU3MTU5Mw@@._V1_.jpg" },
                    { 5, "Daniel Jacob Radcliffe was born on July 23, 1989 in Fulham, London, England, to casting agent Marcia Gresham (née Jacobson) and literary agent Alan Radcliffe. His father is from a Northern Irish Protestant background, while his mother was born in South Africa, to a Jewish family (from Lithuania, Poland, Russia, and Germany).", new DateTime(1989, 7, 23, 0, 0, 0, 0, DateTimeKind.Unspecified), "Daniel", "M", "Radcliffe", null, "Fulham, England", "https://m.media-amazon.com/images/M/MV5BMTg4NTExODc3Nl5BMl5BanBnXkFtZTgwODUyMDEzMDE@._V1_.jpg" },
                    { 6, "Sir Michael Gambon was born in Cabra, Dublin, Ireland, to Mary (Hoare), a seamstress, and Edward Gambon, an engineer. After joining the National Theatre, under the Artistic Directorship of Sir Laurence Olivier, Gambon went on to appear in a number of leading roles in plays written by Alan Ayckbourn.", new DateTime(1940, 10, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), "Michael", "M", "Gambon", null, "Dublin, Ireland", "https://m.media-amazon.com/images/M/MV5BMTY3OTc4MTgyN15BMl5BanBnXkFtZTcwNTAxNjA3Mg@@._V1_.jpg" }
                });

            migrationBuilder.InsertData(
                table: "Characters",
                columns: new[] { "CharacterID", "Alias", "FullName", "Gender", "Picture" },
                values: new object[,]
                {
                    { 1, "Frodo, Mr. Underhill", "Frodo Baggins", "M", "https://dunesjedi.files.wordpress.com/2019/01/frodo_0.jpg?w=1108" },
                    { 2, "Greenleaf", "Legolas", "M", "https://vignette.wikia.nocookie.net/lotr/images/3/33/Legolas_-_in_Two_Towers.PNG/revision/latest/top-crop/width/360/height/450?cb=20120916035151" },
                    { 3, "Elessar, Telcontar, Thorongil, Estel", "Aragorn II Elessar Telcontar", "M", "https://vignette.wikia.nocookie.net/lotr/images/b/b6/Aragorn_profile.jpg/revision/latest?cb=20170121121423" },
                    { 4, "Your Headship, Professorhead, Dumbly - dorr, Crook - nosed, Muggle - loving fool, Crackpot old fool", "Albus Percival Wulfric Brian Dumbledore", "M", "https://i.pinimg.com/236x/38/fd/53/38fd53a85ba7d2431698599ba147239b--david-albus-dumbledore.jpg" },
                    { 5, "The Boy Who Lived, The Chosen One", "Harry James Potter", "M", "https://media.harrypotterfanzone.com/spellbook-thomas-taylor-illustration.jpg" }
                });

            migrationBuilder.InsertData(
                table: "Directors",
                columns: new[] { "DirectorID", "DOB", "Firsname", "Lastname" },
                values: new object[,]
                {
                    { 1, new DateTime(1961, 10, 31, 0, 0, 0, 0, DateTimeKind.Unspecified), "Peter", "Jackson" },
                    { 2, new DateTime(1958, 9, 10, 0, 0, 0, 0, DateTimeKind.Unspecified), "Chris", "Columbus" },
                    { 3, new DateTime(1963, 10, 8, 0, 0, 0, 0, DateTimeKind.Unspecified), "David", "Yates" }
                });

            migrationBuilder.InsertData(
                table: "Franchises",
                columns: new[] { "FranchiseID", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "The Lord of the Rings is a film series of three epic fantasy adventure films directed by Peter Jackson, based on the novel written by J. R. R. Tolkien.", "Lord of The Rings" },
                    { 2, "Harry Potter is a film series based on the eponymous novels by author J. K. Rowling. The series is distributed by Warner Bros. and consists of eight fantasy films, beginning with Harry Potter and the Philosopher's Stone (2001) and culminating with Harry Potter and the Deathly Hallows – Part 2 (2011).", "Harry Potter" }
                });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "MovieID", "DirectorID", "FranchiseID", "Genres", "Picture", "ReleaseYear", "Title", "Trailer" },
                values: new object[,]
                {
                    { 1, 1, 1, "Action,Adventure,Drama,Fantasy", "https://m.media-amazon.com/images/M/MV5BN2EyZjM3NzUtNWUzMi00MTgxLWI0NTctMzY4M2VlOTdjZWRiXkEyXkFqcGdeQXVyNDUzOTQ5MjY@._V1_SY999_CR0,0,673,999_AL_.jpg", new DateTime(2001, 12, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), "The Lord of the Rings: The Fellowship of the Ring", "https://www.youtube.com/watch?v=V75dMMIW2B4" },
                    { 2, 1, 1, "Action,Adventure,Drama,Fantasy", "https://m.media-amazon.com/images/M/MV5BZGMxZTdjZmYtMmE2Ni00ZTdkLWI5NTgtNjlmMjBiNzU2MmI5XkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY1000_CR0,0,642,1000_AL_.jpg", new DateTime(2002, 12, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), "The Lord of the Rings: The Two Towers", "https://youtu.be/hYcw5ksV8YQ" },
                    { 3, 1, 1, "Action,Adventure,Drama,Fantasy", "https://m.media-amazon.com/images/M/MV5BNzA5ZDNlZWMtM2NhNS00NDJjLTk4NDItYTRmY2EwMWZlMTY3XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_SY1000_CR0,0,675,1000_AL_.jpg", new DateTime(2003, 12, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), "The Lord of the Rings: The Return of the King", "https://youtu.be/r5X-hFf6Bwo" },
                    { 4, 2, 2, "Adventure,Family,Fantasy", "https://m.media-amazon.com/images/M/MV5BNjQ3NWNlNmQtMTE5ZS00MDdmLTlkZjUtZTBlM2UxMGFiMTU3XkEyXkFqcGdeQXVyNjUwNzk3NDc@._V1_.jpg", new DateTime(2001, 11, 16, 0, 0, 0, 0, DateTimeKind.Unspecified), "Harry Potter and the Sorcerer's Stone", "https://youtu.be/VyHV0BRtdxo" },
                    { 5, 3, 2, "Adventure,Family,Fantasy", "https://m.media-amazon.com/images/M/MV5BMjIyZGU4YzUtNDkzYi00ZDRhLTljYzctYTMxMDQ4M2E0Y2YxXkEyXkFqcGdeQXVyNTIzOTk5ODM@._V1_SX667_CR0,0,667,999_AL_.jpg", new DateTime(2011, 7, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), "Harry Potter and the Deathly Hallows: Part 2", "https://youtu.be/mObK5XD8udk" }
                });

            migrationBuilder.InsertData(
                table: "MovieCharacters",
                columns: new[] { "CharacterID", "ActorID", "MovieID", "MovieCharacterID", "Picture" },
                values: new object[,]
                {
                    { 1, 1, 1, 0, "https://giantbomb1.cbsistatic.com/uploads/scale_small/3/39164/1245387-untitled_1.png" },
                    { 2, 2, 1, 0, "https://i.pinimg.com/originals/04/80/29/048029f362c484a2a46b928afbe98837.jpg" },
                    { 3, 3, 1, 0, "https://cdn.vox-cdn.com/thumbor/06nk-xAPGzm1jbuYDkucCbNnbzA=/1400x1400/filters:format(jpeg)/cdn.vox-cdn.com/uploads/chorus_asset/file/10856645/aragorn.jpg" },
                    { 1, 1, 2, 0, "https://giantbomb1.cbsistatic.com/uploads/scale_small/3/39164/1245387-untitled_1.png" },
                    { 2, 2, 2, 0, "https://i.pinimg.com/originals/04/80/29/048029f362c484a2a46b928afbe98837.jpg" },
                    { 3, 3, 2, 0, "https://cdn.vox-cdn.com/thumbor/06nk-xAPGzm1jbuYDkucCbNnbzA=/1400x1400/filters:format(jpeg)/cdn.vox-cdn.com/uploads/chorus_asset/file/10856645/aragorn.jpg" },
                    { 1, 1, 3, 0, "https://giantbomb1.cbsistatic.com/uploads/scale_small/3/39164/1245387-untitled_1.png" },
                    { 2, 2, 3, 0, "https://i.pinimg.com/originals/04/80/29/048029f362c484a2a46b928afbe98837.jpg" },
                    { 3, 3, 3, 0, "https://cdn.vox-cdn.com/thumbor/06nk-xAPGzm1jbuYDkucCbNnbzA=/1400x1400/filters:format(jpeg)/cdn.vox-cdn.com/uploads/chorus_asset/file/10856645/aragorn.jpg" },
                    { 4, 4, 4, 0, "https://i.ytimg.com/vi/5MzQifNc_Oo/maxresdefault.jpg" },
                    { 5, 5, 4, 0, "https://seasideblogsong.files.wordpress.com/2014/05/harry-potter.jpg" },
                    { 4, 6, 5, 0, "https://cdn3.whatculture.com/images/2016/09/5c633db213a54ab2-600x338.jpg" },
                    { 5, 5, 5, 0, "https://hips.hearstapps.com/digitalspyuk.cdnds.net/11/21/550w_movies_harry_potter_it_all_ends.jpg" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "MovieCharacters",
                keyColumns: new[] { "CharacterID", "ActorID", "MovieID" },
                keyValues: new object[] { 1, 1, 1 });

            migrationBuilder.DeleteData(
                table: "MovieCharacters",
                keyColumns: new[] { "CharacterID", "ActorID", "MovieID" },
                keyValues: new object[] { 1, 1, 2 });

            migrationBuilder.DeleteData(
                table: "MovieCharacters",
                keyColumns: new[] { "CharacterID", "ActorID", "MovieID" },
                keyValues: new object[] { 1, 1, 3 });

            migrationBuilder.DeleteData(
                table: "MovieCharacters",
                keyColumns: new[] { "CharacterID", "ActorID", "MovieID" },
                keyValues: new object[] { 2, 2, 1 });

            migrationBuilder.DeleteData(
                table: "MovieCharacters",
                keyColumns: new[] { "CharacterID", "ActorID", "MovieID" },
                keyValues: new object[] { 2, 2, 2 });

            migrationBuilder.DeleteData(
                table: "MovieCharacters",
                keyColumns: new[] { "CharacterID", "ActorID", "MovieID" },
                keyValues: new object[] { 2, 2, 3 });

            migrationBuilder.DeleteData(
                table: "MovieCharacters",
                keyColumns: new[] { "CharacterID", "ActorID", "MovieID" },
                keyValues: new object[] { 3, 3, 1 });

            migrationBuilder.DeleteData(
                table: "MovieCharacters",
                keyColumns: new[] { "CharacterID", "ActorID", "MovieID" },
                keyValues: new object[] { 3, 3, 2 });

            migrationBuilder.DeleteData(
                table: "MovieCharacters",
                keyColumns: new[] { "CharacterID", "ActorID", "MovieID" },
                keyValues: new object[] { 3, 3, 3 });

            migrationBuilder.DeleteData(
                table: "MovieCharacters",
                keyColumns: new[] { "CharacterID", "ActorID", "MovieID" },
                keyValues: new object[] { 4, 4, 4 });

            migrationBuilder.DeleteData(
                table: "MovieCharacters",
                keyColumns: new[] { "CharacterID", "ActorID", "MovieID" },
                keyValues: new object[] { 4, 6, 5 });

            migrationBuilder.DeleteData(
                table: "MovieCharacters",
                keyColumns: new[] { "CharacterID", "ActorID", "MovieID" },
                keyValues: new object[] { 5, 5, 4 });

            migrationBuilder.DeleteData(
                table: "MovieCharacters",
                keyColumns: new[] { "CharacterID", "ActorID", "MovieID" },
                keyValues: new object[] { 5, 5, 5 });

            migrationBuilder.DeleteData(
                table: "Actors",
                keyColumn: "ActorID",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Actors",
                keyColumn: "ActorID",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Actors",
                keyColumn: "ActorID",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Actors",
                keyColumn: "ActorID",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Actors",
                keyColumn: "ActorID",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Actors",
                keyColumn: "ActorID",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "CharacterID",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "CharacterID",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "CharacterID",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "CharacterID",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "CharacterID",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "MovieID",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "MovieID",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "MovieID",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "MovieID",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "MovieID",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Directors",
                keyColumn: "DirectorID",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Directors",
                keyColumn: "DirectorID",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Directors",
                keyColumn: "DirectorID",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Franchises",
                keyColumn: "FranchiseID",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Franchises",
                keyColumn: "FranchiseID",
                keyValue: 2);
        }
    }
}
